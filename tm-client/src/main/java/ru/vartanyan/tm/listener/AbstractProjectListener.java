package ru.vartanyan.tm.listener;

import org.jetbrains.annotations.NotNull;
import org.springframework.beans.factory.annotation.Autowired;
import ru.vartanyan.tm.endpoint.ProjectEndpoint;

public abstract class AbstractProjectListener extends AbstractListener {

    @NotNull
    @Autowired
    protected ProjectEndpoint projectEndpoint;

}
