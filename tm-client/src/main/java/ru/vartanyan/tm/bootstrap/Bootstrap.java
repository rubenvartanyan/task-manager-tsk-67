package ru.vartanyan.tm.bootstrap;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import lombok.SneakyThrows;
import org.jetbrains.annotations.Nullable;
import org.jetbrains.annotations.NotNull;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.ApplicationEventPublisher;
import org.springframework.stereotype.Component;
import ru.vartanyan.tm.api.service.*;
import ru.vartanyan.tm.event.ConsoleEvent;
import ru.vartanyan.tm.listener.AbstractListener;
import ru.vartanyan.tm.listener.system.ExitListener;
import ru.vartanyan.tm.component.FileScanner;
import ru.vartanyan.tm.endpoint.*;
import ru.vartanyan.tm.util.SystemUtil;
import ru.vartanyan.tm.util.TerminalUtil;

import java.io.File;
import java.lang.Exception;
import java.nio.charset.StandardCharsets;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.Arrays;

@Setter
@Getter
@Component
@NoArgsConstructor
public final class Bootstrap {

    @NotNull
    @Autowired
    private ApplicationEventPublisher publisher;

    @NotNull
    @Autowired
    public ILoggerService loggerService;

    @Autowired
    public AbstractListener[] listeners;

    @NotNull
    @Autowired
    private FileScanner fileScanner;

    @Nullable
    private SessionDTO session = null;

    @SneakyThrows
    public void initPID() {
        final String fileName = "task-manager.pid";
        final String pid = Long.toString(SystemUtil.getPID());
        Files.write(Paths.get(fileName), pid.getBytes(StandardCharsets.UTF_8));
        final File file = new File(fileName);
        file.deleteOnExit();
    }

    public void process() {
        while (true){
            System.out.println();
            System.out.println("ENTER COMMAND:");
            @NotNull final String command = TerminalUtil.nextLine();
            loggerService.command(command);
            try {
                publisher.publishEvent(new ConsoleEvent(command));
                System.out.println("[OK]");
            } catch (final Exception e) {
                loggerService.error(e);
                System.err.println("[FAIL]");
            }
        }
    }

    public boolean parseArgs(@Nullable String[] args) throws Exception {
        if (args == null || args.length == 0) return false;
        @NotNull final String arg = args[0];
        parseArg(arg);
        return true;
    }

    private void parseArg(@Nullable final String arg) throws Exception {
        if (arg == null || arg.isEmpty()) return;
        @Nullable String command = null;
        for (@NotNull final AbstractListener listener : listeners) {
            if (arg.equals(listener.arg())) command = listener.name();
        }
        if (command == null) return;
        publisher.publishEvent(new ConsoleEvent(command));
    }

    public void showIncorrectArg() {
        System.out.println("Error! Argument not found...");
    }

    private void init() {
        initPID();
        fileScanner.init();
    }

    public static void showIncorrectCommand() {
        System.out.println("Error! Command not found...");
    }

    public void run(final String... args) throws Exception {
        init();
        if (parseArgs(args)) System.exit(0);
        process();
    }

}
