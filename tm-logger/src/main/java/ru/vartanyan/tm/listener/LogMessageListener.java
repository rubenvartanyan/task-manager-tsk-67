package ru.vartanyan.tm.listener;


import lombok.AllArgsConstructor;
import org.jetbrains.annotations.NotNull;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import org.springframework.stereotype.Service;
import ru.vartanyan.tm.api.service.ILoggingService;
import ru.vartanyan.tm.service.LoggingService;

import javax.jms.Message;
import javax.jms.MessageListener;
import javax.jms.ObjectMessage;

@Component
@AllArgsConstructor
public class LogMessageListener implements MessageListener {

    @NotNull
    @Autowired
    final ILoggingService loggingService;

    @Override
    public void onMessage(Message message) {
        if (message instanceof ObjectMessage) {
            loggingService.writeLog(message);
        }
    }

}
