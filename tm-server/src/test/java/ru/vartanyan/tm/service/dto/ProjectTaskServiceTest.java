package ru.vartanyan.tm.service.dto;

import org.jetbrains.annotations.NotNull;
import org.junit.After;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.junit.experimental.categories.Category;
import org.springframework.context.annotation.AnnotationConfigApplicationContext;
import ru.vartanyan.tm.api.service.dto.IProjectService;
import ru.vartanyan.tm.api.service.dto.IProjectTaskService;
import ru.vartanyan.tm.api.service.dto.ITaskService;
import ru.vartanyan.tm.api.service.dto.IUserService;
import ru.vartanyan.tm.configuration.ServerConfiguration;
import ru.vartanyan.tm.dto.Project;
import ru.vartanyan.tm.dto.Task;
import ru.vartanyan.tm.dto.User;
import ru.vartanyan.tm.marker.DBCategory;
import ru.vartanyan.tm.service.TestUtil;

public class ProjectTaskServiceTest {

    @NotNull AnnotationConfigApplicationContext context =
            new AnnotationConfigApplicationContext(ServerConfiguration.class);

    @NotNull
    private final IProjectService projectService = context.getBean(IProjectService.class);

    @NotNull
    private final IUserService userService = context.getBean(IUserService.class);

    @NotNull
    private final ITaskService taskService = context.getBean(ITaskService.class);

    @NotNull
    private final IProjectTaskService projectTaskService = context.getBean(IProjectTaskService.class);

    @Before
    public void before() {
        TestUtil.initUser();
    }

    @After
    public void after() {
        userService.deleteAll();
    }

    @Test
    @Category(DBCategory.class)
    public void bindTaskByProjectIdTest() {
        final Task task = new Task();
        final @NotNull User user = userService.findOneByLogin("test");
        final String userId = user.getId();
        final Project project = projectService.add(userId, "testBind", "-");
        final String projectId = project.getId();
        final String taskId = task.getId();
        task.setUserId(userId);
        taskService.add(task);
        projectTaskService.bindTaskByProject(userId, projectId, taskId);
        Assert.assertTrue(!(taskService.findOneByIdAndUserId(userId, taskId) == null));
    }

    @Test
    @Category(DBCategory.class)
    public void findAllByProjectIdTest() {
        final Task task = new Task();
        final @NotNull User user = userService.findOneByLogin("test");
        final String userId = user.getId();
        final Project project = projectService.add(userId, "testFindAll", "-");
        final String projectId = project.getId();
        task.setUserId(userId);
        task.setProjectId(projectId);
        taskService.add(task);
        Assert.assertFalse(projectTaskService.findAllByProjectIdAndUserId(projectId, userId).isEmpty());
        Assert.assertEquals(1, projectTaskService.findAllByProjectIdAndUserId(projectId, userId).size());

        final Task task2 = new Task();
        task2.setUserId(userId);
        task2.setProjectId(projectId);
        taskService.add(task2);
        Assert.assertEquals(2, projectTaskService.findAllByProjectIdAndUserId(projectId, userId).size());

        final Task task3 = new Task();
        final @NotNull User user2 = userService.findOneByLogin("test2");
        final String user2Id = user2.getId();
        task3.setUserId(user2Id);
        task3.setProjectId(projectId);
        taskService.add(task3);
        Assert.assertEquals(2, projectTaskService.findAllByProjectIdAndUserId(projectId, userId).size());
        Assert.assertEquals(1, projectTaskService.findAllByProjectIdAndUserId(projectId, user2Id).size());

        final Task task4 = new Task();
        final Project project2 = projectService.add(userId, "testFindAll2", "-");
        final String project2Id = project2.getId();
        task4.setUserId(userId);
        task4.setProjectId(project2Id);
        taskService.add(task4);
        Assert.assertEquals(2, projectTaskService.findAllByProjectIdAndUserId(projectId, userId).size());
        Assert.assertEquals(1, projectTaskService.findAllByProjectIdAndUserId(project2Id, userId).size());
    }

    @Test
    @Category(DBCategory.class)
    public void removeAllByProjectIdTest() {
        final Task task = new Task();
        final Task task2 = new Task();
        final Task task3 = new Task();
        final @NotNull User user = userService.findOneByLogin("test");
        final String userId = user.getId();
        final Project project = projectService.add(userId, "testBind", "-");
        final String projectId = project.getId();
        task.setUserId(userId);
        task.setProjectId(projectId);
        task2.setUserId(userId);
        task2.setProjectId(projectId);
        task3.setUserId(userId);
        task3.setProjectId(projectId);
        taskService.add(task);
        taskService.add(task2);
        taskService.add(task3);
        Assert.assertEquals(3, projectTaskService.findAllByProjectIdAndUserId(projectId, userId).size());
        projectTaskService.removeProjectById(userId, projectId);
        Assert.assertTrue(projectTaskService.findAllByProjectIdAndUserId(userId, projectId).isEmpty());
    }

    @Test
    @Category(DBCategory.class)
    public void unbindTaskFromProjectIdTest() {
        final Task task = new Task();
        final @NotNull User user = userService.findOneByLogin("test");
        final String userId = user.getId();
        final String taskId = task.getId();
        task.setUserId(userId);
        taskService.add(task);
        projectTaskService.unbindTaskFromProject(userId, taskId);
        Assert.assertTrue(!(taskService.findOneByIdAndUserId(userId, taskId) == null));
        projectTaskService.unbindTaskFromProject(userId, taskId);
        final Task task2 = taskService.findOneByIdAndUserId(userId, taskId);
        Assert.assertNull(task2.getProjectId());
    }

}
