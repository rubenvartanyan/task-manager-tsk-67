package ru.vartanyan.tm.exception.incorrect;

public class IncorrectIndexException extends Exception{

    public IncorrectIndexException() {
        super("Error! Incorrect index...");
    }

}
