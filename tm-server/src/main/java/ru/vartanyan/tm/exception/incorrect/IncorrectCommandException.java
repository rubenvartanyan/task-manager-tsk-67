package ru.vartanyan.tm.exception.incorrect;

import org.jetbrains.annotations.NotNull;

public class IncorrectCommandException extends Exception{

    public IncorrectCommandException(@NotNull final String command) {
        super("Error! " + command + " is incorrect command...");
    }

}