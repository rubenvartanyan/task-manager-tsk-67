package ru.vartanyan.tm.repository.model;

import org.hibernate.jpa.QueryHints;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Repository;
import ru.vartanyan.tm.api.repository.IRepositoryGraph;
import ru.vartanyan.tm.model.ProjectGraph;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import java.util.List;

public interface IProjectRepositoryGraph extends IRepositoryGraph<ProjectGraph> {

    void deleteAllByUserId(@NotNull String userId);

    @Nullable
    ProjectGraph findOneById(@Nullable final String id);

    ProjectGraph findOneByNameAndUserId(@Nullable final String userId,
                                        @Nullable final String name);

    void removeOneById(@Nullable final String id);

    @NotNull
    List<ProjectGraph> findAll();

    @NotNull
    List<ProjectGraph> findAllByUserId(@Nullable final String userId);

    @Nullable
    ProjectGraph findOneByIdAndUserId(
            @Nullable final String userId,
            @Nullable final String id
    );

    void removeOneByNameAndUserId(@Nullable final String userId,
                                  @Nullable final String name);

    void deleteAll();

    void removeOneByIdAndUserId(@Nullable final String userId,
                                @NotNull final String id);

}
