package ru.vartanyan.tm.repository.model;

import org.hibernate.jpa.QueryHints;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.springframework.context.annotation.Scope;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;
import ru.vartanyan.tm.api.repository.IRepositoryGraph;
import ru.vartanyan.tm.model.TaskGraph;

import javax.persistence.EntityManager;
import java.util.List;

public interface ITaskRepositoryGraph extends IRepositoryGraph<TaskGraph>  {

     @Nullable
     TaskGraph findOneById(@Nullable final String id);

     void removeOneById(@Nullable final String id);

    @NotNull
    List<TaskGraph> findAll();

    @NotNull
    List<TaskGraph> findAllByUserId(String userId);

    @NotNull
    List<TaskGraph> findAllByProjectIdAndUserId(@Nullable final String userId,
                                                @Nullable final String projectId);

    void removeAllByProjectIdAndUserId(
            @Nullable final String userId,
            @Nullable final String projectId
    );

    @Modifying
    @Query("UPDATE Task e SET e.projectId = :projectId WHERE e.userId = :userId AND e.id = :taskId")
    void bindTaskByProjectId(
            @Param("userId") @NotNull final String userId,
            @Param("projectId") @NotNull final String projectId,
            @Param("taskId") @NotNull final String taskId
    );

    @Modifying
    @Query("UPDATE Task e SET e.projectId = NULL WHERE e.userId = :userId AND e.id = :id")
    void unbindTaskFromProjectId(@Param("userId") @NotNull final String userId,
                                 @Param("id") @NotNull final String id);

    @Nullable TaskGraph findOneByIdAndUserId(
            @Nullable final String userId,
            @Nullable final String id
    );

    @Nullable TaskGraph findOneByNameAndUserId(
            @Nullable final String userId,
            @Nullable final String name
    );

    void removeOneByNameAndUserId(@Nullable final String userId,
                                  @Nullable final String name);

    void deleteAllByUserId(@Nullable final String userId);

    void removeOneByIdAndUserId(@Nullable final String userId,
                                @Nullable final String id);

}
