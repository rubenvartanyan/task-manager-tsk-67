package ru.vartanyan.tm.api.repository;

import org.jetbrains.annotations.NotNull;
import org.springframework.data.jpa.repository.JpaRepository;
import ru.vartanyan.tm.model.AbstractEntityGraph;

import javax.persistence.TypedQuery;

public interface IRepositoryGraph<E extends AbstractEntityGraph> extends JpaRepository<E, String> {

}
