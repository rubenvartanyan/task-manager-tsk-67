package ru.vartanyan.tm.api.service.model;

import lombok.SneakyThrows;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.vartanyan.tm.enumerated.Status;
import ru.vartanyan.tm.model.TaskGraph;

import java.util.List;

public interface ITaskServiceGraph extends IBusinessServiceGraph<TaskGraph> {

    @NotNull
    TaskGraph add(
            @Nullable String userId,
            @Nullable String name,
            @Nullable String description
    );

    @SneakyThrows
    void remove(@Nullable TaskGraph entity);

    @SneakyThrows
    void clear(@Nullable String userId);

    @Nullable
    @SneakyThrows
    List<TaskGraph> findAll(@Nullable String userId);

    @SneakyThrows
    @Nullable TaskGraph findOneByUserIdAndId(
            @Nullable String userId,
            @Nullable String id
    );

    @Nullable
    @SneakyThrows
    TaskGraph findOneByIndex(
            @Nullable String userId,
            @Nullable Integer index
    );

    @Nullable
    @SneakyThrows
    TaskGraph findOneByNameAndUserId(
            @Nullable String userId,
            @Nullable String name
    );

    @SneakyThrows
    void remove(
            @Nullable String userId,
            @Nullable TaskGraph entity
    );

    @SneakyThrows
    void removeOneById(
            @Nullable String userId,
            @Nullable String id
    );

    @SneakyThrows
    void removeOneByIndex(
            @Nullable String userId,
            @Nullable Integer index
    );

    @SneakyThrows
    void removeOneByName(
            @Nullable String userId,
            @Nullable String name
    );

    @SneakyThrows
    void changeStatusById(
            @Nullable String userId,
            @Nullable String id,
            @Nullable Status status
    );

    @SneakyThrows
    void updateById(
            @Nullable String userId,
            @Nullable String id,
            @Nullable String name,
            @Nullable String description
    );
}
