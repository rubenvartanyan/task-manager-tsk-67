package ru.vartanyan.tm.endpoint;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import ru.vartanyan.tm.api.endpoint.ITaskEndpoint;
import ru.vartanyan.tm.api.service.dto.IProjectTaskService;
import ru.vartanyan.tm.api.service.dto.ISessionService;
import ru.vartanyan.tm.api.service.dto.ITaskService;
import ru.vartanyan.tm.dto.Session;
import ru.vartanyan.tm.dto.Task;
import ru.vartanyan.tm.exception.system.AccessDeniedException;

import javax.jws.WebMethod;
import javax.jws.WebParam;
import javax.jws.WebService;
import java.util.List;

import javax.xml.bind.annotation.XmlRootElement;

@XmlRootElement(name="yourRootElementName")

@Component
@WebService
public final class TaskEndpoint extends AbstractEndpoint implements ITaskEndpoint {

    @NotNull
    @Autowired
    private ISessionService sessionService;

    @NotNull
    @Autowired
    private ITaskService taskService;

    @NotNull
    @Autowired
    private IProjectTaskService projectTaskService;

    @NotNull
    @Override
    @WebMethod
    public Task addTask(
            @WebParam(name = "session", partName = "session") @Nullable final Session session,
            @WebParam(name = "name", partName = "name") @NotNull final String name,
            @WebParam(name = "description", partName = "description") @NotNull final String description
    ) throws AccessDeniedException {
        sessionService.validate(session);
        if (session == null) throw new AccessDeniedException();
        return taskService.add(session.getUserId(), name, description);
    }

    @Override
    @WebMethod
    public void clearBySessionTask(
            @WebParam(name = "session", partName = "session") @Nullable final Session session
    ) throws AccessDeniedException {
        sessionService.validate(session);
        if (session == null) throw new AccessDeniedException();
        if (session.getUserId() == null) throw new AccessDeniedException();
        taskService.clear(session.getUserId());
    }

    @Override
    @NotNull
    @WebMethod
    public List<Task> findAllTask(
            @WebParam(name = "session", partName = "session") @Nullable final Session session
    ) throws AccessDeniedException, AccessDeniedException {
        sessionService.validate(session);
        if (session == null) throw new AccessDeniedException();
        return taskService.findAll(session.getUserId());
    }

    @Override
    @Nullable
    @WebMethod
    public Task findTaskOneById(
            @WebParam(name = "session", partName = "session") @Nullable final Session session,
            @WebParam(name = "id", partName = "id") @NotNull final String id
    ) throws AccessDeniedException, AccessDeniedException {
        sessionService.validate(session);
        if (session == null) throw new AccessDeniedException();
        return taskService.findOneByIdAndUserId(session.getUserId(), id);
    }

    @Override
    @Nullable
    @WebMethod
    public Task findTaskOneByIndex(
            @WebParam(name = "session", partName = "session") @Nullable final Session session,
            @WebParam(name = "index", partName = "index") @NotNull final Integer index
    ) throws AccessDeniedException, AccessDeniedException {
        sessionService.validate(session);
        if (session == null) throw new AccessDeniedException();
        return taskService.findOneByIndex(session.getUserId(), index);
    }

    @Override
    @Nullable
    @WebMethod
    public Task findTaskOneByName(
            @WebParam(name = "session", partName = "session") @Nullable final Session session,
            @WebParam(name = "name", partName = "name") @NotNull final String name
    ) throws AccessDeniedException, AccessDeniedException {
        sessionService.validate(session);
        if (session == null) throw new AccessDeniedException();
        return taskService.findOneByName(session.getUserId(), name);
    }

    @Override
    @WebMethod
    public void removeTask(
            @WebParam(name = "session", partName = "session") @Nullable final Session session,
            @WebParam(name = "task", partName = "task") @NotNull Task task
    ) throws AccessDeniedException, AccessDeniedException {
        sessionService.validate(session);
        if (session == null) throw new AccessDeniedException();
        taskService.remove(session.getUserId(), task);
    }

    @Override
    @WebMethod
    public void removeTaskOneById(
            @WebParam(name = "session", partName = "session") @Nullable final Session session,
            @WebParam(name = "id", partName = "id") @NotNull final String id
    ) throws AccessDeniedException, AccessDeniedException {
        sessionService.validate(session);
        if (session == null) throw new AccessDeniedException();
        taskService.removeOneById(session.getUserId(), id);
    }

    @Override
    @WebMethod
    public void removeTaskOneByIndex(
            @WebParam(name = "session", partName = "session") @Nullable final Session session,
            @WebParam(name = "index", partName = "index") @NotNull final Integer index
    ) throws AccessDeniedException, AccessDeniedException {
        sessionService.validate(session);
        if (session == null) throw new AccessDeniedException();
        taskService.removeOneByIndex(session.getUserId(), index);
    }

    @Override
    @WebMethod
    public void removeTaskOneByName(
            @WebParam(name = "session", partName = "session") @Nullable final Session session,
            @WebParam(name = "name", partName = "name") @NotNull final String name
    ) throws AccessDeniedException, AccessDeniedException {
        sessionService.validate(session);
        if (session == null) throw new AccessDeniedException();
        taskService.removeOneByName(session.getUserId(), name);
    }

    @Override
    @WebMethod
    public void finishTaskById(
            @WebParam(name = "session", partName = "session") @Nullable final Session session,
            @WebParam(name = "id", partName = "id") @NotNull final String id
    ) throws AccessDeniedException, AccessDeniedException {
        sessionService.validate(session);
        if (session == null) throw new AccessDeniedException();
        taskService.finishById(session.getUserId(), id);
    }

    @Override
    @WebMethod
    public void finishTaskByIndex(
            @WebParam(name = "session", partName = "session") @Nullable final Session session,
            @WebParam(name = "index", partName = "index") @NotNull final Integer index
    ) throws AccessDeniedException, AccessDeniedException {
        sessionService.validate(session);
        if (session == null) throw new AccessDeniedException();
        taskService.finishByIndex(session.getUserId(), index);
    }

    @Override
    @WebMethod
    public void finishTaskByName(
            @WebParam(name = "session", partName = "session") @Nullable final Session session,
            @WebParam(name = "name", partName = "name") @NotNull final String name
    ) throws AccessDeniedException, AccessDeniedException {
        sessionService.validate(session);
        if (session == null) throw new AccessDeniedException();
        taskService.finishByName(session.getUserId(), name);
    }

    @Override
    @WebMethod
    public void startTaskById(
            @WebParam(name = "session", partName = "session") @Nullable final Session session,
            @WebParam(name = "id", partName = "id") @NotNull final String id
    ) throws AccessDeniedException, AccessDeniedException {
        sessionService.validate(session);
        if (session == null) throw new AccessDeniedException();
        taskService.startById(session.getUserId(), id);
    }

    @Override
    @WebMethod
    public void startTaskByIndex(
            @WebParam(name = "session", partName = "session") @Nullable final Session session,
            @WebParam(name = "index", partName = "index") @NotNull final Integer index
    ) throws AccessDeniedException, AccessDeniedException {
        sessionService.validate(session);
        if (session == null) throw new AccessDeniedException();
        taskService.startByIndex(session.getUserId(), index);
    }

    @Override
    @WebMethod
    public void startTaskByName(
            @WebParam(name = "session", partName = "session") @Nullable final Session session,
            @WebParam(name = "name", partName = "name") @NotNull final String name
    ) throws AccessDeniedException, AccessDeniedException {
        sessionService.validate(session);
        if (session == null) throw new AccessDeniedException();
        taskService.startByName(session.getUserId(), name);
    }

    @Override
    @WebMethod
    public void updateTaskById(
            @WebParam(name = "session", partName = "session") @Nullable final Session session,
            @WebParam(name = "id", partName = "id") @NotNull final String id,
            @WebParam(name = "name", partName = "name") @NotNull final String name,
            @WebParam(name = "description", partName = "description") @NotNull final String description
    ) throws AccessDeniedException, AccessDeniedException {
        sessionService.validate(session);
        if (session == null) throw new AccessDeniedException();
        taskService.updateById(session.getUserId(), id, name, description);
    }

    @Override
    @WebMethod
    public void updateTaskByIndex(
            @WebParam(name = "session", partName = "session") @Nullable final Session session,
            @WebParam(name = "index", partName = "index") @NotNull final Integer index,
            @WebParam(name = "name", partName = "name") @NotNull final String name,
            @WebParam(name = "description", partName = "description") @NotNull final String description
    ) throws AccessDeniedException, AccessDeniedException {
        sessionService.validate(session);
        if (session == null) throw new AccessDeniedException();
        taskService.updateByIndex(session.getUserId(), index, name, description);
    }

    @Override
    @WebMethod
    public void bindTaskByProject(
            @WebParam(name = "session", partName = "session") @Nullable final Session session,
            @WebParam(name = "projectId", partName = "projectId") @Nullable final String projectId,
            @WebParam(name = "taskId", partName = "taskId") @Nullable final String taskId
    ) throws AccessDeniedException, AccessDeniedException {
        sessionService.validate(session);
        if (session == null) throw new AccessDeniedException();
        projectTaskService.bindTaskByProject(session.getUserId(), projectId, taskId);
    }

    @Override
    @NotNull
    @WebMethod
    public List<Task> findAllByProjectId(
            @WebParam(name = "session", partName = "session") @Nullable final Session session,
            @WebParam(name = "projectId", partName = "projectId") @Nullable final String projectId
    ) throws AccessDeniedException, AccessDeniedException {
        sessionService.validate(session);
        if (session == null) throw new AccessDeniedException();
        return projectTaskService.findAllByProjectIdAndUserId(session.getUserId(), projectId);
    }

    @Override
    @WebMethod
    public void unbindTaskFromProject(
            @WebParam(name = "session", partName = "session") @Nullable final Session session,
            @WebParam(name = "taskId", partName = "taskId") @Nullable final String taskId
    ) throws AccessDeniedException, AccessDeniedException {
        sessionService.validate(session);
        if (session == null) throw new AccessDeniedException();
        projectTaskService.unbindTaskFromProject(session.getUserId(), taskId);
    }

}
