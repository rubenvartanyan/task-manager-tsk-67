package ru.vartanyan.tm.service.dto;

import lombok.SneakyThrows;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import ru.vartanyan.tm.api.service.dto.IProjectService;
import ru.vartanyan.tm.dto.Project;
import ru.vartanyan.tm.enumerated.Status;
import ru.vartanyan.tm.exception.empty.EmptyIdException;
import ru.vartanyan.tm.exception.empty.EmptyLoginException;
import ru.vartanyan.tm.exception.empty.EmptyNameException;
import ru.vartanyan.tm.exception.incorrect.IncorrectIndexException;
import ru.vartanyan.tm.exception.system.NullObjectException;
import ru.vartanyan.tm.repository.dto.IProjectRepository;

import java.util.List;

@Service
public final class ProjectService extends AbstractService<Project> implements
        IProjectService {

    @NotNull
    @Autowired
    public IProjectRepository projectRepository;

    @NotNull
    public IProjectRepository getProjectRepository() {
        return projectRepository;
    }

    @Override
    @SneakyThrows
    @Transactional
    public void add(@Nullable final Project entity) {
        if (entity == null) throw new NullObjectException();
        @NotNull final IProjectRepository projectRepository = getProjectRepository();
        projectRepository.save(entity);
    }

    @Override
    @SneakyThrows
    @Transactional
    public Project add(
            @Nullable final String userId,
            @Nullable final String name,
            @Nullable final String description
    ) {
        if (userId == null) throw new EmptyIdException();
        if (name == null) throw new EmptyNameException();
        if (description == null) throw new EmptyLoginException();
        @NotNull final Project project = new Project();
        project.setName(name);
        project.setDescription(description);
        project.setUserId(userId);
        @NotNull IProjectRepository projectRepository = getProjectRepository();
        projectRepository.save(project);
        return project;
    }

    @Override
    @SneakyThrows
    @Transactional
    public void addAll(@Nullable List<Project> entities) {
        if (entities == null) throw new NullObjectException();
        @NotNull final IProjectRepository projectRepository = getProjectRepository();
        entities.forEach(projectRepository::save);
    }

    @Override
    @SneakyThrows
    @Transactional
    public void deleteAll() {
        @NotNull final IProjectRepository projectRepository = getProjectRepository();
        projectRepository.deleteAll();
    }

    @Override
    @SneakyThrows
    @Transactional
    public void remove(@Nullable final Project entity) {
        if (entity == null) throw new NullObjectException();
        @NotNull final IProjectRepository projectRepository = getProjectRepository();
        projectRepository.deleteById(entity.getId());
    }

    @NotNull
    @Override
    @SneakyThrows
    public List<Project> findAll() {
        @NotNull final IProjectRepository projectRepository = getProjectRepository();
        return projectRepository.findAll();
    }

    @Nullable
    @Override
    @SneakyThrows
    @Transactional
    public Project findOneById(
            @Nullable final String id
    ) {
        @NotNull final IProjectRepository projectRepository = getProjectRepository();
        if (id == null) throw new EmptyIdException();
        return projectRepository.findOneById(id);
    }

    @SneakyThrows
    @Override
    @Transactional
    public void removeOneById(
            @Nullable final String id
    ) {
        if (id == null) throw new EmptyIdException();
        @NotNull final IProjectRepository projectRepository = getProjectRepository();
        projectRepository.deleteById(id);
    }

    @SneakyThrows
    @Override
    @Transactional
    public void clear(@Nullable final String userId) {
        if (userId == null) throw new EmptyIdException();
        @NotNull final IProjectRepository projectRepository = getProjectRepository();
        projectRepository.deleteAllByUserId(userId);
    }

    @NotNull
    @SneakyThrows
    @Override
    @Transactional
    public List<Project> findAll(@Nullable final String userId) {
        if (userId == null) throw new EmptyIdException();
        @NotNull final IProjectRepository projectRepository = getProjectRepository();
        return projectRepository.findAllByUserId(userId);
    }

    @SneakyThrows
    @Override
    @Transactional
    public @Nullable Project findOneById(
            @Nullable final String userId, @Nullable final String id
    ) {
        if (userId == null) throw new EmptyIdException();
        if (id == null) throw new EmptyIdException();
        @NotNull final IProjectRepository projectRepository = getProjectRepository();
        return projectRepository.findOneByIdAndUserId(userId, id);
    }

    @NotNull
    @SneakyThrows
    @Override
    @Transactional
    public Project findOneByIndex(
            @Nullable final String userId,
            @Nullable final Integer index
    ) {
        if (userId == null) throw new EmptyIdException();
        if (index == null || index < 0) throw new IncorrectIndexException();
        @NotNull final IProjectRepository projectRepository = getProjectRepository();
        return projectRepository.findAllByUserId(userId).get(index);
    }

    @SneakyThrows
    @Override
    @Transactional
    public @Nullable Project findOneByNameAndUserId(
            @Nullable final String userId,
            @Nullable final String name
    ) {
        if (userId == null) throw new EmptyIdException();
        if (name == null) throw new EmptyNameException();
        @NotNull final IProjectRepository projectRepository = getProjectRepository();
        return projectRepository.findOneByNameAndUserId(userId, name);
    }

    @SneakyThrows
    @Override
    @Transactional
    public void remove(
            @Nullable final String userId,
            @Nullable final Project entity
    ) {
        if (userId == null) throw new EmptyIdException();
        if (entity == null) throw new NullObjectException();
        @NotNull final IProjectRepository projectRepository = getProjectRepository();
        projectRepository.removeOneByIdAndUserId(userId, entity.getId());
    }

    @SneakyThrows
    @Override
    @Transactional
    public void removeOneById(
            @Nullable final String userId,
            @Nullable final String id
    ) {
        if (userId == null) throw new EmptyIdException();
        if (id == null) throw new EmptyIdException();
        @NotNull final IProjectRepository projectRepository = getProjectRepository();
        projectRepository.removeOneByIdAndUserId(userId, id);
    }

    @SneakyThrows
    @Override
    @Transactional
    public void removeOneByIndex(
            @Nullable final String userId,
            @Nullable final Integer index
    ) {
        if (userId == null) throw new EmptyIdException();
        if (index == null || index == 0 || index < 0) throw new IncorrectIndexException();
        @NotNull final IProjectRepository projectRepository = getProjectRepository();
        @NotNull Project project = projectRepository.findAllByUserId(userId).get(index);
        getProjectRepository().removeOneByIdAndUserId(userId, project.getId());

    }

    @SneakyThrows
    @Override
    @Transactional
    public void removeOneByName(
            @Nullable final String userId, @Nullable final String name
    ) {
        if (userId == null) throw new EmptyIdException();
        if (name == null) throw new EmptyNameException();
        @NotNull final IProjectRepository projectRepository = getProjectRepository();
        projectRepository.removeOneByNameAndUserId(userId, name);
    }

    @SneakyThrows
    @Override
    @Transactional
    public void changeStatusById(
            @Nullable final String userId,
            @Nullable final String id,
            @Nullable final Status status
    ) {
        if (userId == null) throw new EmptyIdException();
        if (id == null) throw new EmptyIdException();
        if (status == null) throw new NullObjectException();
        @NotNull final Project entity = findOneById(userId, id);
        @NotNull final IProjectRepository projectRepository = getProjectRepository();
        if (entity == null) throw new NullObjectException();
        entity.setStatus(status);
        projectRepository.save(entity);
    }

    @SneakyThrows
    @Override
    @Transactional
    public void changeStatusByIndex(
            @Nullable final String userId,
            @Nullable final Integer index,
            @Nullable final Status status
    ) {
        if (userId == null) throw new EmptyIdException();
        if (index == null || index == 0 || index < 0) throw new IncorrectIndexException();
        if (status == null) throw new NullObjectException();
        @NotNull final Project entity = findOneByIndex(userId, index);
        if (entity == null) throw new NullObjectException();
        entity.setStatus(status);
        @NotNull final IProjectRepository projectRepository = getProjectRepository();
        projectRepository.save(entity);
    }

    @SneakyThrows
    @Override
    @Transactional
    public void changeStatusByName(
            @Nullable final String userId,
            @Nullable final String name,
            @Nullable final Status status
    ) {
        if (userId == null) throw new EmptyIdException();
        if (name == null) throw new EmptyNameException();
        if (status == null) throw new NullObjectException();
        @NotNull final Project entity = findOneByNameAndUserId(userId, name);
        if (entity == null) throw new NullObjectException();
        entity.setStatus(status);
        @NotNull final IProjectRepository projectRepository = getProjectRepository();
        projectRepository.save(entity);
    }

    @SneakyThrows
    @Override
    @Transactional
    public void finishById(
            @Nullable final String userId,
            @Nullable final String id
    ) {
        if (userId == null) throw new EmptyIdException();
        if (id == null) throw new EmptyIdException();
        @NotNull final Project entity = findOneById(userId, id);
        if (entity == null) throw new NullObjectException();
        entity.setStatus(Status.COMPLETE);
        @NotNull final IProjectRepository projectRepository = getProjectRepository();
        projectRepository.save(entity);
    }

    @SneakyThrows
    @Override
    @Transactional
    public void finishByIndex(
            @Nullable final String userId,
            @Nullable final Integer index
    ) {
        if (userId == null) throw new EmptyIdException();
        if (index == null || index == 0 || index < 0) throw new IncorrectIndexException();
        @NotNull final Project entity = findOneByIndex(userId, index);
        if (entity == null) throw new NullObjectException();
        entity.setStatus(Status.COMPLETE);
        @NotNull final IProjectRepository projectRepository = getProjectRepository();
        projectRepository.save(entity);
    }

    @SneakyThrows
    @Override
    @Transactional
    public void finishByName(
            @Nullable final String userId,
            @Nullable final String name
    ) {
        if (userId == null) throw new EmptyIdException();
        if (name == null) throw new EmptyNameException();
        @NotNull final Project entity = findOneByNameAndUserId(userId, name);
        if (entity == null) throw new NullObjectException();
        @NotNull final IProjectRepository projectRepository = getProjectRepository();
        entity.setStatus(Status.COMPLETE);
        projectRepository.save(entity);
    }

    @SneakyThrows
    @Override
    @Transactional
    public void startById(
            @Nullable final String userId,
            @Nullable final String id
    ) {
        if (userId == null) throw new EmptyIdException();
        if (id == null) throw new EmptyIdException();
        @NotNull final Project entity = findOneById(userId, id);
        if (entity == null) throw new NullObjectException();
        entity.setStatus(Status.IN_PROGRESS);
        @NotNull final IProjectRepository projectRepository = getProjectRepository();
        projectRepository.save(entity);
    }

    @SneakyThrows
    @Override
    @Transactional
    public void startByIndex(
            @Nullable final String userId,
            @Nullable final Integer index
    ) {
        if (userId == null) throw new EmptyIdException();
        if (index == null || index == 0 || index < 0) throw new IncorrectIndexException();
        @NotNull final Project entity = findOneByIndex(userId, index);
        if (entity == null) throw new NullObjectException();
        @NotNull final IProjectRepository projectRepository = getProjectRepository();
        entity.setStatus(Status.IN_PROGRESS);
        projectRepository.save(entity);
    }

    @SneakyThrows
    @Override
    @Transactional
    public void startByName(
            @Nullable final String userId,
            @Nullable final String name
    ) {
        if (userId == null) throw new EmptyIdException();
        if (name == null) return;
        @NotNull final Project entity = findOneByNameAndUserId(userId, name);
        if (entity == null) throw new NullObjectException();
        @NotNull final IProjectRepository projectRepository = getProjectRepository();
        entity.setStatus(Status.IN_PROGRESS);
        projectRepository.save(entity);
    }

    @SneakyThrows
    @Override
    @Transactional
    public void updateById(
            @Nullable final String userId,
            @Nullable final String id,
            @Nullable final String name,
            @Nullable final String description
    ) {
        if (userId == null) throw new EmptyIdException();
        if (id == null) throw new EmptyIdException();
        if (name == null) throw new EmptyNameException();
        @NotNull final Project entity = findOneById(userId, id);
        if (entity == null) throw new NullObjectException();
        @NotNull final IProjectRepository projectRepository = getProjectRepository();
        entity.setName(name);
        entity.setDescription(description);
        projectRepository.save(entity);
    }

    @Override
    public void deleteAllByUserId(@Nullable String userId) {
        projectRepository.deleteAllByUserId(userId);
    }

    @SneakyThrows
    @Override
    @Transactional
    public void updateByIndex(
            @Nullable final String userId,
            @Nullable final Integer index,
            @Nullable final String name,
            @Nullable final String description
    ) {
        if (userId == null) throw new EmptyIdException();
        if (index == 0 || index == null || index < 0) throw new IncorrectIndexException();
        if (name == null) throw new EmptyNameException();
        @NotNull final Project entity = findOneByIndex(userId, index);
        if (entity == null) throw new NullObjectException();
        @NotNull final IProjectRepository projectRepository = getProjectRepository();
        entity.setName(name);
        entity.setDescription(description);
        projectRepository.save(entity);
    }

}
