package ru.vartanyan.tm.endpoint;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;
import ru.vartanyan.tm.api.endpoint.IProjectEndpoint;
import ru.vartanyan.tm.api.service.IProjectService;
import ru.vartanyan.tm.model.Project;

import java.util.ArrayList;
import java.util.Collection;
import java.util.List;

@RestController
@RequestMapping("/api/projects")
public class ProjectEndpoint implements IProjectEndpoint {

    @Autowired
    private IProjectService projectService;

    @Override
    @GetMapping("/findAll")
    public List<Project> findAll() {
        return new ArrayList<>(projectService.findAll());
    }

    @Override
    @GetMapping("/find/{id}")
    public Project find(@PathVariable("id") String id) {
        return projectService.findById(id);
    }

    @Override
    @PostMapping("/create")
    public Project create(@RequestBody Project project) {
        projectService.add(project);
        return project;
    }

    @Override
    @PostMapping("/createAll")
    public List<Project> createAll(@RequestBody List<Project> projects) {
        for (Project project: projects) {
            projectService.add(project);
        }
        return projects;
    }

    @Override
    @PutMapping("/save")
    public Project save(@RequestBody Project project) {
        projectService.add(project);
        return project;
    }

    @Override
    @PutMapping("/saveAll")
    public List<Project> saveAll(@RequestBody List<Project> projects) {
        for (Project project: projects) {
            projectService.add(project);
        }
        return projects;
    }

    @Override
    @DeleteMapping("/delete/{id}")
    public void delete(@PathVariable("id") String id) {
        projectService.removeById(id);
    }

    @Override
    @DeleteMapping("/deleteAll")
    public void deleteAll() {
        final Collection<Project> projects = projectService.findAll();
        for (Project project: projects) {
            projectService.removeById(project.getId());
        }
    }

}
