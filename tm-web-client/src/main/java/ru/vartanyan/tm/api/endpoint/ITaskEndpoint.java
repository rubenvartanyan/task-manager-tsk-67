package ru.vartanyan.tm.api.endpoint;

import org.springframework.web.bind.annotation.*;
import ru.vartanyan.tm.model.Task;

import javax.jws.WebService;
import java.util.List;

@WebService
@RequestMapping("/api/tasks")
public interface ITaskEndpoint {
    @GetMapping("/findAll")
    List<Task> findAll();

    @GetMapping("/find/{id}")
    Task find(@PathVariable("id") String id);

    @PostMapping("/create")
    Task create(@RequestBody Task task);

    @PostMapping("/createAll")
    List<Task> createAll(@RequestBody List<Task> tasks);

    @PostMapping("/save")
    Task save(@RequestBody Task task);

    @PostMapping("/saveAll")
    List<Task> saveAll(@RequestBody List<Task> tasks);

    @PostMapping("/delete/{id}")
    void delete(@PathVariable("id") String id);

    @PostMapping("/deleteAll")
    void deleteAll();
}
