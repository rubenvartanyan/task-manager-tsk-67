package ru.vartanyan.tm.api.service;

import org.jetbrains.annotations.NotNull;

public interface IPropertyService {

    @NotNull
    String getApplicationVersion();

    @NotNull
    Integer getPasswordIteration();

    @NotNull
    String getPasswordSecret();

    @NotNull
    Integer getSignIteration();

    @NotNull
    String getSignSecret();

    @NotNull String getCacheProviderConfig();

    @NotNull String getCacheRegionFactory();

    @NotNull String getCacheRegionPrefix();

    @NotNull
    String getDeveloperEmail();

    @NotNull
    String getDeveloperName();

    @NotNull
    String getDialect();

    @NotNull
    String getHbm2ddlAuto();

    @NotNull
    String getJdbcDriver();

    @NotNull
    String getJdbcPassword();

    @NotNull
    String getJdbcUrl();

    @NotNull
    String getJdbcUser();

    @NotNull
    String getServerHost();

    @NotNull
    String getServerPort();

    @NotNull
    String getShowSql();

    @NotNull
    String getPackagesToScan1();

    @NotNull
    String getPackagesToScan2();

    @NotNull String getUseLiteMemberValue();

    @NotNull String getUseMinimalPuts();

    @NotNull String getUseQueryCache();

    @NotNull String getUseSecondLevelCache();

}
