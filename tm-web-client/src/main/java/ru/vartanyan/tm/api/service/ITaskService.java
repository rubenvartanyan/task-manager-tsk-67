package ru.vartanyan.tm.api.service;

import ru.vartanyan.tm.model.Task;

public interface ITaskService extends IRecordService<Task> {

}
