package ru.vartanyan.tm.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import ru.vartanyan.tm.model.Task;

public interface ITaskRepository extends JpaRepository<Task, String> {


}
