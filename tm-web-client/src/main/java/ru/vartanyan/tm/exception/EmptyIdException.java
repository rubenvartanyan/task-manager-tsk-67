package ru.vartanyan.tm.exception;

public class EmptyIdException extends AbstractException {

    public EmptyIdException() {
        super("Error! Id cannot be null or empty...");
    }

}
